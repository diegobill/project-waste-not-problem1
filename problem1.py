from timeit import timeit

func = 'fib(93)'

py = timeit(
    func,
    number=1_000_000,
    setup='from fib_py import fib'
)

print(f'Function {func} takes {py} seconds to run')